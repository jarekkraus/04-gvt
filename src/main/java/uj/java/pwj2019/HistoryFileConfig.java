package uj.java.pwj2019;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

public class HistoryFileConfig {

    private File historyFile;

    public static HistoryFileConfig historyFileConfig(String path) throws IOException {
        return new HistoryFileConfig(path);
    }

    public HistoryFileConfig(String path) throws IOException {
        if(Files.notExists(Paths.get(path))) {
            new File(path).createNewFile();
        }
        historyFile = new File(path);
    }

    public void setData(int numberVersion, String action) throws IOException {
        FileWriter writer = new FileWriter(historyFile,true);
        writer.write(numberVersion + ": " + action + "\n");
        writer.close();
    }

    public char getLastNumberVersion() throws IOException {
        Scanner scan = new Scanner(historyFile);
        String result = "";
        while(scan.hasNextLine()){
            result = scan.nextLine();
        }
        scan.close();
        return result.charAt(0);
    }

    public void getHistoryNumber(int n) throws IOException {
        BufferedReader b = new BufferedReader(new FileReader(".gvt/history.ini"));
        ArrayList<String> lines = new ArrayList<String>();

        String readLine = "";

        while ((readLine = b.readLine()) != null) {
            lines.add(readLine);
        }
        b.close();

        for (int i = lines.size() - 1; i >= lines.size() - n; i--) {
            System.out.println(lines.get(i));
        }
    }

    public void getHistory() throws IOException {
        BufferedReader b = new BufferedReader(new FileReader(".gvt/history.ini"));

        String readLine = "";

        while ((readLine = b.readLine()) != null) {
            System.out.println(readLine);
        }
        b.close();
    }
}
