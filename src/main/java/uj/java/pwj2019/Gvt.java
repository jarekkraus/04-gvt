package uj.java.pwj2019;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Gvt {

    private static void commandEvaluation(String[] args) throws IOException {
        String command = args[0];

        if (!command.equals("init") && Files.notExists(Paths.get("./.gvt"))) {
            System.out.print("Current directory is not initialized. Please use \"init\" command to initialize.");
            System.exit(-2);
        }

        if (command.equals("init")) {
            if (Files.exists(Paths.get(".gvt"))) {
                System.out.print("Current directory is already initialized.");
                System.exit(10);
            } else {
                try {
                    init();
                } catch (Throwable e) {
                    System.out.println("Underlying system problem. See ERR for details.");
                    e.printStackTrace();
                    System.exit(-3);
                }
            }
        } else {

            switch (command) {
                case "checkout":
                    if (args[1].matches("\\d+") && Files.exists(Paths.get(".gvt/version_" + args[1]))) {
                        File my_repo = new File("../my_repo");
                        File version = new File(".gvt/version_" + args[1]);
                        Version.copyVersion(version, my_repo);
                        System.out.print("Version " + args[1] + " checked out successfully.");
                    } else {
                        System.out.print("Invalid version number: " + args[1]);
                        System.exit(40);
                    }
                    break;
                case "detach":
                    if (args.length == 1 || (args.length <= 3 && args[1].equals("-m"))) {
                        System.out.print("Please specify file to detach.\n");
                        System.exit(30);
                    } else if (Files.notExists(Paths.get(".gvt/version_" + HistoryFileConfig.historyFileConfig(".gvt/history.ini").getLastNumberVersion() + "/" + args[1]))) {
                        System.out.print("File " + args[1] + " is not added to gvt.\n");
                    } else {
                        try {
                            if (args.length > 2) {
                                detach(args[1], args[3]);
                            } else {
                                detach(args[1], "");
                            }

                        } catch (Throwable e) {
                            System.out.println("File " + args[1] + " cannot be detached, see ERR for details.");
                            e.printStackTrace();
                            System.exit(31);
                        }
                    }
                    break;
                case "commit":
                    if (args.length == 1 || (args.length <= 3 && args[1].equals("-m"))) {
                    System.out.print("Please specify file to commit.");
                        System.exit(50);
                    } else if (Files.notExists(Paths.get(args[1]))) {
                        System.out.print("File " + args[1] + " does not exist.");
                        System.exit(51);
                    } else if (Files.notExists(Paths.get(".gvt/version_" + HistoryFileConfig.historyFileConfig(".gvt/history.ini").getLastNumberVersion() + "/" + args[1]))) {
                        System.out.print("File " + args[1] + " is not added to gvt.");
                    } else {
                        try {
                            if (args.length > 2) {
                                commit(args[1], args[3]);
                            } else {
                                commit(args[1], "");
                            }

                        } catch (Throwable e) {
                            System.out.println("File " + args[1] + " cannot be commited, see ERR for details.");
                            e.printStackTrace();
                            System.exit(22);
                        }
                    }
                    break;
                case "add":
                    if (args.length == 1 || (args.length <= 3 && args[1].equals("-m"))) {
                        System.out.print("Please specify file to add.");
                        System.exit(20);
                    } else if (Files.notExists(Paths.get(args[1]))) {
                        System.out.print("File " + args[1] + " not found.");
                        System.exit(21);
                    } else if (Files.exists(Paths.get(".gvt/version_" +
                            HistoryFileConfig.historyFileConfig(".gvt/history.ini").getLastNumberVersion() +
                            "/" + args[1]))) {
                        System.out.print("File " + args[1] + " already added.");
                    } else {
                        try {
                            if (args.length > 2) {
                                add(args[1], args[3]);
                            } else {
                                add(args[1], "");
                            }

                        } catch (Throwable e) {
                            System.out.println("File " + args[1] + " cannot be added, see ERR for details.");
                            e.printStackTrace();
                            System.exit(22);
                        }
                    }
                    break;
                case "version":
                    if (args.length > 1) {
                        if (args[1].matches("\\d+") && Files.exists(Paths.get(".gvt/version_" + args[1]))) {
                            VersionFileConfig.versionFileConfig(".gvt/version_" + args[1] + "/version.ini").getData();
                        } else {
                            System.out.print("Invalid version number: " + args[1]);
                            System.exit(60);
                        }
                    } else {
                        VersionFileConfig.versionFileConfig(".gvt/version_" + HistoryFileConfig.historyFileConfig(".gvt/history.ini").getLastNumberVersion() + "/version.ini").getData();
                    }
                    break;
                case "history":
                    if (args.length > 2) {
                        String parameter = args[1];
                        Integer numberOfVersions = Integer.parseInt(args[2]);

                        if (parameter.equals("-last") && numberOfVersions instanceof Integer) {
                            history(numberOfVersions);
                            break;
                        }
                    }
                    history((int) Files.lines(Paths.get(".gvt/history.ini")).count());
                    break;
                default:
                    System.out.print("Unknown command " + command);
                    System.exit(1);
            }
        }

    }

    private static void detach(String fileName, String message) throws IOException {

        Version.newVersion().detach(fileName, "Detached file: " + fileName, message);

        System.out.print("File " + fileName + " detached successfully.\n");
    }

    private static void commit(String fileName, String message) throws IOException {

        Version.newVersion().add(fileName, "Committed file: " + fileName, message);

        System.out.print("File " + fileName + " committed successfully.\n");
    }

    private static void add(String fileName, String message) throws IOException {

        Version.newVersion().add(fileName,"Added file: " + fileName, message);

        System.out.print("File " + fileName + " added successfully.\n");
    }

    private static void history(Integer numberOfVersions) throws IOException {
        int versionNumber = Character.getNumericValue(HistoryFileConfig.historyFileConfig(".gvt/history.ini").getLastNumberVersion());
        if (numberOfVersions < versionNumber + 1) {
            HistoryFileConfig.historyFileConfig(".gvt/history.ini").getHistoryNumber(numberOfVersions);
        } else {
            HistoryFileConfig.historyFileConfig(".gvt/history.ini").getHistory();
        }
    }

    private static void init() throws IOException {
        new File(".gvt/version_0").mkdirs();

        VersionFileConfig.versionFileConfig(".gvt/version_0/version.ini").setData(0, "GVT initialized.");
        HistoryFileConfig.historyFileConfig(".gvt/history.ini").setData(0,"GVT initialized.");

        System.out.print("Current directory initialized successfully.");
    }

    public static void main(String[] args) throws IOException {
        if (args.length == 0) {
            System.out.print("Please specify command.");
            System.exit(1);
        } else {
            commandEvaluation(args);
        }
    }
}
