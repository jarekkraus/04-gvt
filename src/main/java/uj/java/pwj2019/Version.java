package uj.java.pwj2019;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

public class Version {

    int numberNewVersion;

    public static Version newVersion() throws IOException {
        return new Version();
    }

    public Version() throws IOException {

        int numberLastVersion = Character.getNumericValue(HistoryFileConfig.historyFileConfig(".gvt/history.ini").getLastNumberVersion());
        numberNewVersion = numberLastVersion + 1;

        File filesLastVersion = new File(".gvt/version_" + numberLastVersion);
        File filesNewVersion = new File(".gvt/version_" + numberNewVersion);

        copyVersion(filesLastVersion, filesNewVersion);
    }

    public void add(String fileName, String action, String message) throws IOException {
        File fileToAdd = new File(fileName);
        File fileInNewVersion = new File(".gvt/version_" + numberNewVersion + "/" + fileName);

        copyFile(fileToAdd,fileInNewVersion);

        HistoryFileConfig.historyFileConfig(".gvt/history.ini").setData(numberNewVersion,action);
        VersionFileConfig.versionFileConfig(".gvt/version_" + numberNewVersion + "/version.ini").setData(numberNewVersion, action + "\n" + message);

    }

    public void detach(String fileName, String action, String message) throws IOException {

        try{
            File file = new File(".gvt/version_" + numberNewVersion + "/" + fileName);
            file.delete();

        }catch(Exception e){
            e.printStackTrace();
        }

        HistoryFileConfig.historyFileConfig(".gvt/history.ini").setData(numberNewVersion,action);
        VersionFileConfig.versionFileConfig(".gvt/version_" + numberNewVersion + "/version.ini").setData(numberNewVersion, action + "\n" + message);

    }

    public void copyFile(File sourceFile, File destinationFile) throws IOException {
        if(sourceFile.isFile()) {
            if(!destinationFile.exists()) {
                destinationFile.createNewFile();
            }
        }
        Files.copy(sourceFile.toPath(), destinationFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
    }

    public static void copyVersion(File sourceFolder, File destinationFolder) throws IOException {
        if (sourceFolder.isDirectory()) {
            if (!destinationFolder.exists()) {
                destinationFolder.mkdir();
            }

            String files[] = sourceFolder.list();

            for(String file : files) {
                if (!file.equals("version.ini")) {
                    File srcFile = new File(sourceFolder, file);
                    File destFile = new File(destinationFolder, file);

                    copyVersion(srcFile, destFile);
                }
            }
        } else {
            Files.copy(sourceFolder.toPath(), destinationFolder.toPath(), StandardCopyOption.REPLACE_EXISTING);
        }
    }
}
