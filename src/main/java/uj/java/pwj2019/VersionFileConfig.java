package uj.java.pwj2019;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

public class VersionFileConfig {

    private File versionFile;

    public static VersionFileConfig versionFileConfig(String path) throws IOException {
        return new VersionFileConfig(path);
    }

    public VersionFileConfig(String path) throws IOException {
        if(Files.notExists(Paths.get(path))) {
            new File(path).createNewFile();
        }
        versionFile = new File(path);
    }

    public void setData(int numberVersion, String action) throws IOException {
        FileWriter writer = new FileWriter(versionFile,true);
        writer.write("Version: " + numberVersion + "\n" + action);
        writer.close();
    }

    public void getData() {
        try (var r = new BufferedReader(new FileReader(versionFile))) {
            String line;
            String result = "";
            while ((line = r.readLine()) != null) {
                result += line + System.lineSeparator();
            }
            System.out.print(result.trim());
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
